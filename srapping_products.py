from bs4 import BeautifulSoup
import requests as r
import time

def log(status:bool,
        row = '',
        category = '',
        main_page_link = '',
        subcategory = '',
        region_name = '',
        one_year_ago_rank = [],
        two_year_ago_rank = []):

    with open('log.txt','a',encoding='utf-8') as log:
        if status:
            log.write(f"""
            >---[ SUCCESS ;D ]
            | - Crawl n° {row}
            | - category : {category}
            | - main_page_link : {main_page_link}
            | - subcategory : {subcategory_name}
            | - subcategory_link : {subcategory_link}
            | - region_name : {region_name}
            | - one_year_ago_rank : {one_year_ago_rank}
            | - two_year_ago_rank : {two_year_ago_rank}
            |
            | - json : {data}
            """)
        else:
            log.write(f"""
            >---[ ERROR at line {row} ;( ]
            | - category : {category}
            | - main_page_link : {main_page_link}
            | - subcategory : {subcategory_name}
            | - subcategory_link : {subcategory_link}
            |
            """)

##################    START TIMER    ###################
begin = time.time()
print('Scrapping started now!')

info = []
final_result = {
        'category':{
            'link':'',
            'name':'',
            'subcategory':{
                'name':'',
                'link':'',
                'region':{
                    'name':'',
                    'year':{
                        2020:[],
                        2021:[]
                    }
                }
            }
        }
    }

s = r.Session() # This session saves all the cookies to future usage
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'}

main_page_link = 'https://www.savarejo.com.br/gds-guia-de-sortimento'
main_page = s.get(main_page_link,headers = headers)._content
main_page = BeautifulSoup(main_page,'html.parser')

arr_categories_names = main_page.find_all('a',{'role':'tab'})
arr_categories_names = [category_name.h2.text for category_name in arr_categories_names]

##################    CSV INSTANSTIATION    ###################
with open('data_compilation.csv','a',encoding='utf-8') as csv_file:
    csv_file.write(f'"category link","category name","subcategory name","subcategory link","region name","1st 2021","2nd 2021","3rd 2021","4th 2021","5th 2021","6th 2021","7th 2021","8th 2021","9th 2021","10th 2021","1st 2020","2nd 2020","3rd 2020","4th 2020","5th 2020","6th 2020","7th 2020","8th 2020","9th 2020","10th 2020","json"\n')

row = 1 # This gonna be used in logs

##################    MAIN PAGE INFORMATION    ###################
for category in arr_categories_names:
    
    subcategories = main_page.find('div',{'id':category}).select('a')

    for subcategory in subcategories:
        subcategory_name = subcategory.getText()
        subcategory_link = 'https://www.savarejo.com.br' + subcategory.get('href')

        ####################    SPECIFIC PAGE INFORMATION    ####################
        # Fetching the specific page's raw html
        specific_page = s.get(subcategory_link)._content
        specific_page = BeautifulSoup(specific_page,'html.parser')

        # Fetching the code areas:
        arr_code_areas = []
        arr_name_areas = []

        counter = 0 # used as index of arr_name_areas
        try:
            for area in specific_page.find('ul',{'id':'ulMixMarcaCategorias'}).find_all('li'):
                # print(area)
                arr_code_areas.append(area.a.get('id'))
                arr_name_areas.append(area.a.strong.text)
        except:
            print(f'[ ERRO ] - Não foram encontrados dados para a subcategoria : {subcategory_name} <===   ')
            log(False,category,main_page_link,subcategory,region_name,one_year_ago_rank,two_year_ago_rank)
            pass


        # Accessing the different areas
        for area in arr_code_areas:
            data = {'cod':area}
            res = s.post('https://www.savarejo.com.br/cms/Principal/Gps/AjaxListagemMixMarcaCategoria',data=data)
            filtered_specific_page = str(BeautifulSoup(res._content,'html.parser').decode('utf-8')).replace(r'\u003c','<').replace(r'\u003e','>').replace(r"\r\n",'').replace(r'\"','"')
            filtered_specific_page = BeautifulSoup(filtered_specific_page,'html.parser')
            
            # Ranking region
            region_name = arr_name_areas[counter]
            counter += 1

            # one year ago's ranking:
            general_one_year_ago_rank = filtered_specific_page.find('div',{'id':'conteudoInfoEsquerda'}).find_all('li')
            one_year_ago_rank = []

            for product in general_one_year_ago_rank:
                product = product.text

                num = ['1','2','3','4','5','6','7','8','9','10','**','*']
                for n in num:
                    product = product.replace(n,'')
                product = product.lstrip().rstrip()

                one_year_ago_rank.append(product)

            # two years ago's ranking:
            general_two_year_ago_rank = filtered_specific_page.find('div',{'id':'conteudoInfoDireita'}).find_all('li')
            two_year_ago_rank = []

            for product in general_two_year_ago_rank:
                product = product.text

                num = ['1','2','3','4','5','6','7','8','9','10','**','*']
                for n in num:
                    product = product.replace(n,'')
                product = product.lstrip().rstrip()

                two_year_ago_rank.append(product)

            data = {}
            data['category'] = {}
            data['category']['link'] = main_page_link
            data['category']['name'] = category
            data['category']['subcategory'] = {}
            data['category']['subcategory']['name'] = subcategory_name
            data['category']['subcategory']['link'] = subcategory_link
            data['category']['subcategory']['region'] = {}
            data['category']['subcategory']['region']['name'] = region_name
            data['category']['subcategory']['region']['year'] = {}
            data['category']['subcategory']['region']['year']['2021'] = one_year_ago_rank
            data['category']['subcategory']['region']['year']['2020'] = two_year_ago_rank

            log(True,row,category,main_page_link,subcategory,region_name,one_year_ago_rank,two_year_ago_rank)
            
            ##################    CSV LOG    #################
            with open('data_compilation.csv','a',encoding='utf-8') as csv_file:
                while len(one_year_ago_rank) != 10: one_year_ago_rank.append('empty')
                while len(two_year_ago_rank) != 10: two_year_ago_rank.append('empty')

                one_year_ago_rank = [f'"{str(item)}",' for item in one_year_ago_rank]
                two_year_ago_rank = [f'"{str(item)}",' for item in two_year_ago_rank]

                csv_row = f'"{main_page_link}","{category}","{subcategory_name}","{subcategory_link}","{region_name}",'

                for item in one_year_ago_rank: csv_row += item

                for item in two_year_ago_rank: csv_row += item

                csv_row += f'"{str(data)}"\n'
                csv_file.write(csv_row)

                partial_time = int(time.time() - begin)
                row += 1
            
            with open('json_compilation.csv','a',encoding='utf-8') as csv_file2:
                csv_file2.write(f'{str(data)}\n')

            if row in [500,1000,1090]: time.sleep(4)
            print(f"""
            | - Crawl n° {row}
            | - category : {category}
            | - main_page_link : {main_page_link}
            | - subcategory : {subcategory_name}
            | - subcategory_link : {subcategory_link}
            | - region_name : {region_name}
            | - one_year_ago_rank : {one_year_ago_rank}
            | - two_year_ago_rank : {two_year_ago_rank}
            |
            | - {partial_time} seconds elapsed...""")

##################    END TIMER    ###################
end = time.time()
elapsed_time = int(end - begin)
print(f'{elapsed_time} seconds elapsed. Done!')

